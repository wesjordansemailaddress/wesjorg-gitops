variable "keycloak_namespace" {
    type = string
    description = "Namespace to deploy Keycloak into"
}

variable "keycloak_version" {
    type = string
    description = "Keycloak version to deploy"
    default = "11.0.2"
}

variable "keycloak_chart_version" {
    type = string
    description = "Keycloak chart version"
    default = "9.1.0"
}

variable "keycloak_fqdn" {
    type = string
    description = "FQDN for keycloak deployment"
    default = "login.wesj.app"
}

variable "keycloak_admin_user" {
    type = string
    description = "Username for default admin user"
    default = "admin2"
}

variable "keycloak_realm" {
    type = string
    description = "Realm to be provisioned in Keycloak"
}