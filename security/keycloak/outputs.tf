output "admin_user" {
    value = var.keycloak_admin_user
}

output "admin_password" {
    value = random_password.admin_password.result
}