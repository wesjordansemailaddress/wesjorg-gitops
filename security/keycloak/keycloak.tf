resource "random_password" "admin_password" {
  length = 32
  special = true
  override_special = "_%@"
}

resource "kubernetes_secret" "keycloak_admin" {
  metadata {
    name = "keycloak-admin"
    namespace = var.keycloak_namespace
  }

  data = {
    user = var.keycloak_admin_user
    password = random_password.admin_password.result
  }
}

resource "helm_release" "keycloak" {
  name       = "keycloak"
  repository = "https://codecentric.github.io/helm-charts"
  chart      = "keycloak"
  version    = var.keycloak_chart_version
  namespace  = var.keycloak_namespace

  values = [
    "${templatefile("${path.module}/helm-values.yml", {
        keycloak_version = var.keycloak_version
        keycloak_fqdn = var.keycloak_fqdn
        keycloak_namespace = var.keycloak_namespace
        keycloak_admin_secret = kubernetes_secret.keycloak_admin.metadata[0].name
      })}"
  ]

  set {
    name  = "replicas"
    value = "1"
  }
}