resource "kubernetes_deployment" "blog" {
  metadata {
    name = "blog"
    namespace = var.blog_namespace
  }

  spec {
    replicas = var.blog_replicas

    selector {
      match_labels = {
        app = "blog"
      }
    }

    template {
      metadata {
        labels = {
          app = "blog"
        }
      }

      spec {
        container {
          name = "blog"
          image = "${var.blog_repo}:${var.blog_tag}"
          image_pull_policy = "Always"

          port {
            container_port = 80
          }
        }

        image_pull_secrets {
          name = var.docker_secret
        }
      }
    }
  }
}

resource "kubernetes_service" "blog" {
  metadata {
    name = "blog"
    namespace = var.blog_namespace
  }

  spec {
    selector = {
      app = "blog"
    }

    port {
      target_port = 80
      port        = 80
    }
  }
}

resource "kubernetes_ingress" "blog" {
  metadata {
    name = "blog"
    namespace = var.blog_namespace
    annotations = {
      "kubernetes.io/ingress.class" = "traefik"
      "cert-manager.io/issuer" = "applications-issuer-letsencrypt-prod"
      "cert-manager.io/acme-challenge-type" = "http01"
    }
  }

  spec {
    rule {
      host = var.blog_fqdn
      http {
        path {
          path = "/"
          backend {
            service_name = "blog"
            service_port = 80
          }
        }
      }
    }
    tls {
      hosts       = [var.blog_fqdn]
      secret_name = "blog-k8s-wesj-org-tls"
    }
  }
}