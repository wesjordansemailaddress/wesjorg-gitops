variable "blog_namespace" {
  type = string
  default = "applications"
}

variable "blog_replicas" {
  default = 1
}

variable "blog_tag" {
  default = "2020.05.09.1720"
}

variable "blog_repo" {
  default = "docker-registry.wesj.app/blog"
}

variable "blog_fqdn" {
  default = "wesj.org"
}

variable "docker_secret" {
  default = "regcred"
}