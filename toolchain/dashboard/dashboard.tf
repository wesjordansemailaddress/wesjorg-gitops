resource "helm_release" "dashboard" {
  name       = "dashboard"
  repository = "https://kubernetes.github.io/dashboard"
  chart      = "kubernetes-dashboard"
  version    = var.dashboard_chart_version
  namespace  = var.dashboard_namespace

  set {
    name  = "image.tag"
    value = "v${var.dashboard_version}"
  }

  set {
    name  = "replicaCount"
    value = var.dashboard_replicas
  }

  set {
    name  = "service.type"
    value = "NodePort"
  }

  set {
    name  = "protocolHttp"
    value = true
  }
}

resource "kubernetes_cluster_role_binding" "dashboard_crb" {
  metadata {
    name = "dashboard-kubernetes-dashboard"
  }

  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  subject {
    kind      = "ServiceAccount"
    name      = "dashboard-kubernetes-dashboard"
    namespace = var.dashboard_namespace
  }
}