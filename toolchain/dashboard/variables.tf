variable "dashboard_namespace" {
    type = string
    description = "Namespace to deploy Dashboard into"
}

variable "dashboard_version" {
    type = string
    description = "Dashboard version to deploy"
    default = "2.0.4"
}

variable "dashboard_chart_version" {
    type = string
    description = "Keycloak chart version"
    default = "2.7.1"
}

variable "dashboard_replicas" {
    default = 1
}