variable "keycloak_realm" {
  type    = string
  default = "wesj.app"
}

variable "keycloak_host" {
  type    = string
  default = "login.wesj.app"
}