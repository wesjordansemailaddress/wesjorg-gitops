terraform {
  required_providers {
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "1.13.2"
    }

    helm = {
      source  = "hashicorp/helm"
      version = "1.3.0"
    }

    random = {
      source  = "hashicorp/random"
      version = "2.3.0"
    }
  }
}

provider "kubernetes" {}

provider "helm" {}

provider "random" {}

module "keycloak" {
  source = "./security/keycloak"

  keycloak_namespace = "security"
  keycloak_fqdn      = var.keycloak_host
  keycloak_realm     = var.keycloak_realm
}

module "dashboard" {
  source = "./toolchain/dashboard"

  dashboard_namespace = "toolchain"
}

module "blog" {
  source = "./applications/blog"
  
  blog_namespace = "applications"
}

output "keycloak_admin_user" {
  value = module.keycloak.admin_user
}

output "keycloak_admin_password" {
  value = module.keycloak.admin_password
}